import java.util.Scanner

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/02/19
* TITLE: Pastisseria I && II
*/

fun main() {

    println("1 -> Pastes\n" +
            "2 -> Begudes")

    val scanner = Scanner(System.`in`)
    when(scanner.nextInt()){
        1 -> {createPasta()}
        2 -> {createDrink()}
    }
}

fun createPasta(){
    class  Pasta(
        val name : String,
        val price : Double,
        val weight : Double,
        val calories : Double
    )
    val croissant = Pasta("CROISSANT",1.15,50.00,406.10)
    println("${croissant.name}\n" +
            "${croissant.price} € \n" +
            "${croissant.weight} g \n" +
            "${croissant.calories} cal \n" )

    val ensaimada = Pasta("ENSAIMADA",1.30,75.00,504.50)
    println("${ensaimada.name}\n" +
            "${ensaimada.price} € \n" +
            "${ensaimada.weight} g \n" +
            "${ensaimada.calories} cal \n")

    val donut = Pasta("DONUT",1.50,60.80,209.90)
    println("${donut.name}\n" +
            "${donut.price} € \n" +
            "${donut.weight} g \n" +
            "${donut.calories} cal \n")

}

fun createDrink(){
    class Drink(
        val name : String,
        var price : Double,
        val sugarIncrement : Boolean
    ) {
        fun increment(price : Double, sugarIncrement : Boolean) : Double{
            return if (sugarIncrement) {
                (price * 110) / 100

            } else {
                price
            }
        }
    }

    val aigua = Drink("AIGUA",1.00, false)
    println("${aigua.name}\n" +
            "${aigua.increment(aigua.price,aigua.sugarIncrement)} € \n" +
            "${aigua.sugarIncrement}\n")

    val cafe = Drink("CAFÉ TALLAT",1.35, false)
    println("${cafe.name}\n" +
            "${cafe.increment(cafe.price,cafe.sugarIncrement)} € \n" +
            "${cafe.sugarIncrement}\n")

    val te = Drink("TÉ VERMELL",1.50, false)
    println("${te.name}\n" +
            "${te.increment(te.price,te.sugarIncrement)} € \n" +
            "${te.sugarIncrement}\n")

    val cola = Drink("COCACOLA",1.65, true)
    println("${cola.name}\n" +
            "${cola.increment(cola.price,cola.sugarIncrement)} € \n" +
            "${cola.sugarIncrement}\n")

}


