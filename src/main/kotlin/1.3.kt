import java.util.Scanner

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/02/19
* TITLE: Carpinteria
*/

class TaulellFusta (
     preuUnitari : Int,
     llargada : Int,
     amplada : Int
) {
    var totalPriceTaulell = 0
    init {
         totalPriceTaulell += preuUnitari * llargada * amplada

    }
}
class LlistoFusta (
     preuUnitari : Int,
     llargada : Int
    ) {
    var totalPriceLlisto = 0
    init {

         totalPriceLlisto += preuUnitari * llargada

    }
}

fun main() {

    var totalPrice = 0


    println("Introdueix el nombre d'elements:")

    val scanner = Scanner(System.`in`)

    for (i in 1..scanner.nextInt()){

        val nom = scanner.next().uppercase()

        if (nom =="TAULELL" ){
            val preuUnitari = scanner.nextInt()
            val llargada = scanner.nextInt()
            val amplada = scanner.nextInt()

            val taulell = TaulellFusta(preuUnitari, llargada, amplada)

            totalPrice += taulell.totalPriceTaulell

        }
        if (nom == "LLISTO" || nom == "LLISTÓ"){

            val preuUnitari = scanner.nextInt()
            val llargada = scanner.nextInt()

            val llisto = LlistoFusta(preuUnitari, llargada)

            totalPrice += llisto.totalPriceLlisto

        }

    }
    println(totalPrice)
}
