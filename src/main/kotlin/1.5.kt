import java.util.Scanner

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/02/19
* TITLE:  MechanicalArmApp
*/

class MechanicArm (
    var turnOn : Boolean,
    var obertura : Double,
    var alcada : Double
        ){

    fun toggle() {
        turnOn = !turnOn
    }

    fun updateAngle(obertura: Int){
        if (turnOn){
            this.obertura += obertura
        }
        if (this.obertura > 360){
            this.obertura -= 360
        }
        if (this.obertura < 0){
            this.obertura += 360
        }

    }
    fun updateAltitude(alcada: Int){
        if (turnOn) {
            this.alcada += alcada
        }
        if (this.alcada > 30){
            this.alcada -= 30
        }
        if (this.alcada < 0){
            this.alcada += 30
        }


    }
}
fun main(){
    val arm = MechanicArm(false,0.0,0.0)

    arm.toggle()
    println("MechanicalArm{openAngle=${arm.obertura},altitude=${arm.alcada},turnedOn=${arm.turnOn}}")

    arm.updateAltitude(3)
    println("MechanicalArm{openAngle=${arm.obertura},altitude=${arm.alcada},turnedOn=${arm.turnOn}}")

    arm.updateAngle(180)
    println("MechanicalArm{openAngle=${arm.obertura},altitude=${arm.alcada},turnedOn=${arm.turnOn}}")

    arm.updateAltitude(-3)
    println("MechanicalArm{openAngle=${arm.obertura},altitude=${arm.alcada},turnedOn=${arm.turnOn}}")

    arm.updateAngle(-180)
    println("MechanicalArm{openAngle=${arm.obertura},altitude=${arm.alcada},turnedOn=${arm.turnOn}}")

    arm.updateAltitude(3)
    println("MechanicalArm{openAngle=${arm.obertura},altitude=${arm.alcada},turnedOn=${arm.turnOn}}")

    arm.toggle()
    println("MechanicalArm{openAngle=${arm.obertura},altitude=${arm.alcada},turnedOn=${arm.turnOn}}")

}
